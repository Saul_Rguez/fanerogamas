# -*- coding: utf-8 -*-
"""
Created on Mon Nov 21 14:45:44 2016
This script runs a simulation on a seagrass population. Mainly based on 
Carr et al.,2012, and, as in this paper, we took parameters and different 
equations from Zharova et al., 2001, and taken some of the data on literture 
they cite suchs as Lawson et al.,2007.(see the report for furhter details).
The model has been simplified by not taken into accoun biomass, as we lack
measures, and we assumed a constant belowground biomass with a ratio allowing
the population growth. We did so because we are mainly interested in temperature
rising and light attenuation effects. For modeling this, we used the approach
for the 490 nm waveleght (Morel, A., & Maritorena, S., 2001), irradiance at the
surface set to 125.8 mol/m²*day according to Tattini et al., 2005, averaged for
annual basis and converted using a 1W/m²=4.6umol/m²*s, and then rescaled on 
daily basis. Temperature and sea level rising taken from IPCC. Temperature 
levels and seasonal varation from Group et al.

Literature:
1.- Carr, J. A., D’Odorico, P., McGlathery, K. J., & Wiberg, P. L. (2012). Modeling the effects of climate change on eelgrass stability and resilience: future scenarios and leading indicators of collapse. Marine Ecology Progress Series, 448, 289-301.
2.- Lawson, S. E., Wiberg, P. L., McGlathery, K. J., & Fugate, D. C. (2007). Wind-driven sediment suspension controls light availability in a shallow coastal lagoon. Estuaries and Coasts, 30(1), 102-112.
3.- Zharova, N., Sfriso, A., Voinov, A., & Pavoni, B. (2001). A simulation model for the annual fluctuation of Zostera marina biomass in the Venice lagoon.Aquatic Botany, 70(2), 135-150.
4.- Morel, A., & Maritorena, S. (2001). Bio-optical properties of oceanic waters- A reappraisal. Journal of Geophysical research, 106(C4), 7163-7180.
5.- Tattini, M., Guidi, L., Morassi‐Bonzi, L., Pinelli, P., Remorini, D., Degl'Innocenti, E., ... & Agati, G. (2005). On the role of flavonoids in the integrated mechanisms of response of Ligustrum vulgare and Phillyrea latifolia to high solar radiation. New Phytologist,167(2), 457-470. 
6.- Group, T. M., de Madron, X. D., Guieu, C., Sempéré, R., Conan, P., Cossa, D., ... & Stemmann, L. (2011). Marine ecosystems’ responses to climatic and anthropogenic forcings in the Mediterranean. Progress in Oceanography, 91(2), 97-166
7.- Pachauri, R. K., Allen, M. R., Barros, V. R., Broome, J., Cramer, W., Christ, R., ... & Dubash, N. K. (2014). Climate change 2014: synthesis Report. Contribution of working groups I, II and III to the fifth assessment report of the intergovernmental panel on climate change (p. 151). IPCC.
@author: saul
"""
#this lines import the anaconda modules necessary for the script to work.
import scipy, scipy.integrate
import numpy as np
import math as math
import matplotlib.pyplot as plt

#this are controls for setting on and off different weather traits, 
#you can set them independenty as you want, True for enable, False for disable
#caps sensitive, capital F and T please...
seasonality = True
temp_rising =True
sea_rising = True
eutrophication = True

#next values are initial conditions, you mess with them as much as you want.
#
#Sets the number to the initial population (nº of shoots).
N=[500]
#set the depths for the simulations, as many as wanted, comma separated values
#please, and dot for decimal separator (m).
depths=(20,25,30,32,34,36)
#set the number of years you want to project(years)
times=30

#next are rates and constants, you mostly don't mess with them, and 
#if you do so note down so you can reset. All the constant and rates taken from
#literature are state are state variables, so they are fixed for every simulation
#run.
#sets the limit for the population for the logistic model 
#(k, maximum population size, nº of shoots)
Nlim = 1000
#sets the water temperature for spring as initial for the model (ºC)
initialtemp=18
#set the temperature change from season to season, have in mind the simulation 
#starts in spring, so this setting means sum for the summer and substract for
#winter. (ºC)
seasonchange=5
#sets the temperature rising per year (ºC/year)
trise=0.011
#sets the sea level rising per year (m/year)
srise=0.0032
#sets the chlorophyle rising rate (mg*m^-3/year)
chlarise=0.007
#sets the chlorophyle initial level (mg*m^-3)
chla=0.7
#irradiance at the surface(mol/m²*day)
Is=125.8
#compensation irradiance (mol/m^2*day)
Ic20=2.4
#saturation irradiance (mol/m^2*day)
Ik20=25.5
#Above- to below-ground biomass ratio limitation, set to 1 if the ratio is <4.0, 
#otherwise set to 0 (fixed to 1 so we allow recruitment,no units)
Rlim=1
#mortality per year(shoots/year)
Nloss=2.375 
#recruitment per year(shoots/year)
maxNgrowth=10.22 
#shape constants for the photosyntesic function (no units)
kophot=0.01
kmphot=0.00001
#optimal and maximum photosynthetic temperatures (ºC)
Topt = 21.5
Tmax = 34
#shape constants for recalculating light saturation lavels (no units)
Thetak=1.04
Thetac=1.17
#light attenuation for pure water(m-1)
kw=0.01660
#shape coefficient for light attenuation (m³/mg??)
xlambda=0.07242
#exponent for light attenuation (¿?)
elambda=0.68955


#this function is the model itself, depending on many constants set before
#and in two more functions. These are gathered toghether by gatherer function
#simplified from Carr et all., not taking into account biomass.
def Ngrowth (N,time):
    dNdt= N*(maxNgrowth*gatherer(initialtemp,time,depth)*(1-(N/Nlim)**2)*Rlim-Nloss)
    return dNdt

#this function gathers the values photosynthesic levels given the temperature
#and irradiance (so depth and attenuation) conditions
def gatherer (temp,time,depth):
    temp=temprise(temp,time)
    temp=seasons (temp, time)
    depth = searise(depth,time)
    kd = eutroph (time)
    photo1=photoTemp (temp)
    photo2=photoLight(depth,temp,kd)
    output=photo1*photo2
    return output

#this function simulates seasonal water temperature changes
def seasons (temp,time):
    if seasonality ==True:
        temp=temp+seasonchange*(math.sin(time*6.28))
    return temp

#this function simulates the water temperature rising.
def temprise (temp,time):
    if temp_rising==True:
        temp=initialtemp+time*trise
    return temp
    
#this function simulates the sea level rising
def searise (depth,time):
    if sea_rising==True:
        depth=depth+time*srise
    return depth
    
#this function simulates eutrophication (mg*m^-3)
def eutroph (time):
    if eutrophication==True:
        kd=kw+xlambda*(chla+chlarise*time)**elambda
    else:
        kd=kw+xlambda*(chla)**elambda
    return kd
    
#this function calculates the photosynthetic rates at a given temperature.
#from Zharova et al.
def photoTemp (temp):
    if temp<= Topt:
        P=kophot**(((Topt-temp)/Topt)**2)
    else:
        P=kmphot**(((temp-Topt)/(Tmax-Topt))**2)
    return P

#this function calculates the level of photosynthetic saturation given the 
#depth and temperature. Zharova et al.
def photoLight (depth,temp,kd):
    Ik=Ik20*Thetak**(temp-20)
    Ic=Ic20*Thetac**(temp-20)
    I=Is*math.exp(-kd*depth)
    if I <= Ic:
       Irr=0
    elif I>= Ik:
        Irr = 1
    else:
        Irr=(I-Ic)/(Ik-Ic)
    return Irr
    
#integrating module, integrates the ecuation Ngrowth for the different depths
#and returns an array with a vector for each depth
vectort = np.arange(0,times,0.05)
array = []
for elemento in depths:
    depth = elemento
    line=scipy.integrate.odeint (Ngrowth,N,vectort)
    array.append(line)

#plotting module, plots each line in the array and adds the stuff such as 
#legend, titles, axis limits...
plt.figure()
for elem in range(0,len(array)):
    plt.plot(vectort,array[elem],label= '%d m' %depths[elem], lw=2.5 )   
lg=plt.legend(loc=0,prop={'size':18},frameon=False)
lg.set_title(title='Depth',prop={'size':22})
lg.draggable(True)

plt.xlabel('Time(years)', fontsize=22)
plt.ylabel('Population (n of shoots/m^2)', fontsize=22)
plt.title('Population size over the time for each depth', fontsize=26)
plt.ylim (0,1000)


#fix so it can be run from the python shell, just in case
plt.show()














